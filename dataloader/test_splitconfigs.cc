#include "../code/catch.hpp"
#include "split_configs.hpp"

TEST_CASE("Num Blocks divisible by Num Ranks") {
  std::vector<int> ranks{0, 1, 2, 3};
  std::vector<int> blocks{1, 2, 3, 4, 5, 6, 7, 8};
  auto const num_ranks = ranks.size();
  auto const num_blocks = blocks.size();
  auto const num_blocks_per_rank = num_blocks/num_ranks;
  std::pair<std::vector<int>::const_iterator, std::vector<int>::const_iterator> se;
  for (auto const r: ranks) {
    se = split_configs(blocks, r, num_ranks);
    CHECK(se.second - se.first == num_blocks_per_rank);
  }
  std::array<std::vector<int>,4> expected;
  expected[0] ={1, 2};
  expected[1] = {3,4};
  expected[2] = {5,6};
  expected[3] = {7,8};  
  for (auto const r: ranks) {
    auto se = split_configs(blocks, r, num_ranks);
    std::vector<int> answers {se.first, se.second};
    CHECK(answers == expected[r]);
  }
}

TEST_CASE("Num Blocks less than Num Ranks") {
  std::vector<int> ranks{0, 1, 2, 3};
  std::vector<int> blocks{1, 2, 3};
  auto const num_ranks = ranks.size();
  auto const num_blocks = blocks.size();

  std::array<std::vector<int>,4> expected;
  expected[0].push_back(1);
  expected[1].push_back(2);
  expected[2].push_back(3);
  for (auto const r: ranks) {
    auto se = split_configs(blocks, r, num_ranks);
    std::vector<int> answers {se.first, se.second};
    CHECK(answers == expected[r]);
  }
}

TEST_CASE("One block and more than one rank") {
  std::vector<int> ranks { 0, 1 };
  std::vector<int> blocks { 1 };
  auto const num_ranks = ranks.size();
  auto const num_blocks = blocks.size();
  std::array<std::vector<int>, 2> expected;
  expected[0].push_back(1);

  for (auto const r : ranks) {
    auto se = split_configs(blocks, r, num_ranks);
    std::vector<int> answers {se.first, se.second};
    CHECK(answers == expected[r]);
  }
}

TEST_CASE("Num Blocks greater than Num ranks and not divisible by Num ranks") {
  std::vector<int> ranks{0, 1, 2};
  std::vector<int> blocks{1, 2, 3, 4};
  auto const num_ranks = ranks.size();
  auto const num_blocks = blocks.size();
  
  std::array<std::vector<int>, 3> expected;
  expected[0] = {1,2};
  expected[1].push_back(3);
  expected[2].push_back(4);

  for (auto const r : ranks) {
    auto se = split_configs(blocks, r, num_ranks);
    std::vector<int> answers {se.first, se.second};
    CHECK(answers == expected[r]);
  }
}
