#include <fstream>
#include <string>
#include <vector>

#include <hepnos/DataSet.hpp>
#include <hepnos/DataStore.hpp>
#include <hepnos/Run.hpp>
#include <hepnos/RunSet.hpp>

#include "dataformat/rec_hdr.hpp"
#include "dataformat/rec_slc.hpp"
#include "dataformat/rec_vtx.hpp"
#include "dataformat/rec_vtx_elastic_fuzzyk.hpp"
#include "dataformat/rec_vtx_elastic_fuzzyk_png.hpp"
#include "dataformat/rec_vtx_elastic_fuzzyk_png_cvnpart.hpp"
#include "dataformat/rec_vtx_elastic_fuzzyk_png_shwlid.hpp"

using namespace std;

void read_datastore();
void write_hdr(hepnos::Run const& r, hepnos::SubRun const& sr);
void write_vtx(hepnos::Run const& r, hepnos::SubRun const& sr);
void write_slc(hepnos::Run const& r, hepnos::SubRun const& sr);
void write_elastic_fuzzyk(hepnos::Run const& r, hepnos::SubRun const& sr);
void write_elastic_fuzzyk_png(hepnos::Run const& r, hepnos::SubRun const& sr);
void write_elastic_fuzzyk_png_shwlid(hepnos::Run const& r,
                                     hepnos::SubRun const& sr);
void write_elastic_fuzzyk_png_cvnpart(hepnos::Run const& r,
                                      hepnos::SubRun const& sr);

void
read_datastore()
{
  hepnos::DataStore const datastore("/tmp/client-config.yaml");
  for (auto it = datastore.begin(); it != datastore.end(); ++it) {
    for (auto const& r : it->runs()) {
      for (auto const& sr : r) {
        write_hdr(r, sr);
        write_vtx(r, sr);
        write_slc(r, sr);
        write_elastic_fuzzyk(r, sr);
        write_elastic_fuzzyk_png(r, sr);
        write_elastic_fuzzyk_png_shwlid(r, sr);
        write_elastic_fuzzyk_png_cvnpart(r, sr);
      }
    }
  }
}

void
write_hdr(hepnos::Run const& r, hepnos::SubRun const& sr)
{
  auto const rn = r.number();
  auto const srn = sr.number();
  ofstream myfile;
  myfile.open(std::to_string(rn) + "_" + std::to_string(srn) + "_hdr_hepnos");
  for (auto const& e : sr) {
    std::vector<ABC::rec_hdr> hdrs;
    e.load("a", hdrs);
    for (auto i = 0; i < hdrs.size(); ++i) {
      myfile << r.number() << '\t' << sr.number() << '\t' << e.number() << '\t'
             << hdrs[i].subevt << '\t' << hdrs[i].second << '\n';
    }
  }
  myfile.close();
}

void
write_vtx(hepnos::Run const& r, hepnos::SubRun const& sr)
{
  ofstream myfile;
  myfile.open("vtx_hepnos");
  for (auto const& e : sr) {
    std::vector<ABC::rec_vtx> vtxs;
    e.load("a", vtxs);
    for (auto i = 0; i < vtxs.size(); ++i) {
      myfile << r.number() << '\t' << sr.number() << '\t' << e.number() << '\t'
             << vtxs[i].subevt << '\t' << vtxs[i].nelastic << '\n';
    }
  }
  myfile.close();
}

void
write_slc(hepnos::Run const& r, hepnos::SubRun const& sr)
{
  ofstream myfile;
  myfile.open("slc_hepnos");
  for (auto const& e : sr) {
    std::vector<ABC::rec_slc> slcs;
    e.load("a", slcs);
    for (auto i = 0; i < slcs.size(); ++i) {
      myfile << r.number() << '\t' << sr.number() << '\t' << e.number() << '\t'
             << slcs[i].subevt << '\t' << slcs[i].nhit << '\n';
    }
  }
  myfile.close();
}

void
write_elastic_fuzzyk(hepnos::Run const& r, hepnos::SubRun const& sr)
{
  ofstream myfile;
  myfile.open("vtx_elastic_fuzzyk_hepnos");
  for (auto const& e : sr) {
    std::vector<ABC::rec_vtx_elastic_fuzzyk> fuzzyk;
    e.load("a", fuzzyk);
    for (auto i = 0; i < fuzzyk.size(); ++i) {
      myfile << r.number() << '\t' << sr.number() << '\t' << e.number() << '\t'
             << fuzzyk[i].subevt << '\t' << fuzzyk[i].rec_vtx_elastic_idx
             << '\t' << fuzzyk[i].npng << '\n';
    }
  }
  myfile.close();
}

void
write_elastic_fuzzyk_png(hepnos::Run const& r, hepnos::SubRun const& sr)
{
  ofstream myfile;
  myfile.open("vtx_elastic_fuzzyk_png_hepnos");
  for (auto const& e : sr) {
    std::vector<ABC::rec_vtx_elastic_fuzzyk_png> fuzzyk;
    e.load("a", fuzzyk);
    for (auto i = 0; i < fuzzyk.size(); ++i) {
      myfile << r.number() << '\t' << sr.number() << '\t' << e.number() << '\t'
             << fuzzyk[i].subevt << '\t' << fuzzyk[i].rec_vtx_elastic_idx
             << '\t' << fuzzyk[i].rec_vtx_elastic_fuzzyk_png_idx << '\t'
             << fuzzyk[i].nhit << '\n';
    }
  }
  myfile.close();
}

void
write_elastic_fuzzyk_png_shwlid(hepnos::Run const& r, hepnos::SubRun const& sr)
{
  ofstream myfile;
  myfile.open("vtx_elastic_fuzzyk_png_shwlid_hepnos");
  for (auto const& e : sr) {
    std::vector<ABC::rec_vtx_elastic_fuzzyk_png_shwlid> fuzzyk;
    e.load("a", fuzzyk);
    for (auto i = 0; i < fuzzyk.size(); ++i) {
      myfile << r.number() << '\t' << sr.number() << '\t' << e.number() << '\t'
             << fuzzyk[i].subevt << '\t' << fuzzyk[i].rec_vtx_elastic_idx
             << '\t' << fuzzyk[i].rec_vtx_elastic_fuzzyk_png_idx << '\t'
             << fuzzyk[i].nplane << '\n';
    }
  }
  myfile.close();
}

void
write_elastic_fuzzyk_png_cvnpart(hepnos::Run const& r, hepnos::SubRun const& sr)
{
  ofstream myfile;
  myfile.open("vtx_elastic_fuzzyk_png_cvnpart_hepnos");
  for (auto const& e : sr) {
    std::vector<ABC::rec_vtx_elastic_fuzzyk_png_cvnpart> fuzzyk;
    e.load("a", fuzzyk);
    for (auto i = 0; i < fuzzyk.size(); ++i) {
      myfile << r.number() << '\t' << sr.number() << '\t' << e.number() << '\t'
             << fuzzyk[i].subevt << '\t' << fuzzyk[i].rec_vtx_elastic_idx
             << '\t' << fuzzyk[i].rec_vtx_elastic_fuzzyk_png_idx << '\t'
             << fuzzyk[i].pdgmax << '\n';
    }
  }
  myfile.close();
}

int
main(int argc, char* argv[])
{
  read_datastore();
  return 0;
}
