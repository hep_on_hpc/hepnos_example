#include <hepnos/DataSet.hpp>
#include <hepnos/DataStore.hpp>


void
create_dataset()
{
  hepnos::DataStore datastore("/tmp/client.yaml");
  auto dset = datastore.createDataSet("nova"); 
}

int
main(int argc, char* argv[])
{
  create_dataset();
  return 0;
}
