import h5py as h5
import numpy as np
import sys

def write_hdr(f):
  r = f["rec.hdr/run"][:]
  assert(np.all(r==r[0]))
  sr = f["rec.hdr/subrun"][:]
  assert(np.all(sr==sr[0]))
  ev = f["rec.hdr/evt"][:]
  sev = f["rec.hdr/subevt"][:]
  sec = f["rec.hdr/second"][:]
  nslc = len(r)
  assert(len(sr) == nslc)
  assert(len(ev) == nslc)
  assert(len(sev) == nslc)
  assert(len(sec) == nslc)

  with open("%d_%d_hdr" % (r[0],sr[0]), "w") as of:
    for i in xrange(nslc):
      of.write("%d\t%d\t%d\t%d\t%d\n" % (r[i], sr[i], ev[i], sev[i], sec[i]))

def write_vtx(f):
  r = f["rec.vtx/run"][:]
  assert(np.all(r==r[0]))
  sr = f["rec.vtx/subrun"][:]
  assert(np.all(sr==sr[0]))
  ev = f["rec.vtx/evt"][:]
  sev = f["rec.vtx/subevt"][:]
  nelastic = f["rec.vtx/nelastic"][:]
  nslc = len(r)
  assert(len(sr) == nslc)
  assert(len(ev) == nslc)
  assert(len(sev) == nslc)
  assert(len(nelastic) == nslc)

  with open("%d_%d_vtx" % (r[0],sr[0]), "w") as of:
    for i in xrange(nslc):
      of.write("%d\t%d\t%d\t%d\t%d\n" % (r[i], sr[i], ev[i], sev[i], nelastic[i]))
    
def write_slc(f):
  r = f["rec.slc/run"][:]
  assert(np.all(r==r[0]))
  sr = f["rec.slc/subrun"][:]
  assert(np.all(sr==sr[0]))
  ev = f["rec.slc/evt"][:]
  sev = f["rec.slc/subevt"][:]
  nhit = f["rec.slc/nhit"][:]
  nslc = len(r)
  assert(len(sr) == nslc)
  assert(len(ev) == nslc)
  assert(len(sev) == nslc)
  assert(len(nhit) == nslc)

  with open("%d_%d_slc" % (r[0],sr[0]), "w") as of:
    for i in xrange(nslc):
      of.write("%d\t%d\t%d\t%d\t%d\n" % (r[i], sr[i], ev[i], sev[i], nhit[i]))

def write_elastic_fuzzyk(f):
  r = f["rec.vtx.elastic.fuzzyk/run"][:]
  assert(np.all(r==r[0]))
  sr = f["rec.vtx.elastic.fuzzyk/subrun"][:]
  assert(np.all(sr==sr[0]))
  ev = f["rec.vtx.elastic.fuzzyk/evt"][:]
  sev = f["rec.vtx.elastic.fuzzyk/subevt"][:]
  nelastic = f["rec.vtx.elastic.fuzzyk/rec.vtx.elastic_idx"][:]
  npng = f["rec.vtx.elastic.fuzzyk/npng"][:]
  nslc = len(r)
  assert(len(sr) == nslc)
  assert(len(ev) == nslc)
  assert(len(sev) == nslc)
  assert(len(nelastic) == nslc)
  assert(len(npng) == nslc)

  with open("%d_%d_vtx_elastic_fuzzyk" % (r[0],sr[0]), "w") as of:
    for i in xrange(nslc):
      of.write("%d\t%d\t%d\t%d\t%d\t%d\n" % (r[i], sr[i], ev[i], sev[i], nelastic[i], npng[i]))

def write_elastic_fuzzyk_png(f):
  r = f["rec.vtx.elastic.fuzzyk.png/run"][:]
  assert(np.all(r==r[0]))
  sr = f["rec.vtx.elastic.fuzzyk.png/subrun"][:]
  assert(np.all(sr==sr[0]))
  ev = f["rec.vtx.elastic.fuzzyk.png/evt"][:]
  sev = f["rec.vtx.elastic.fuzzyk.png/subevt"][:]
  nelastic = f["rec.vtx.elastic.fuzzyk.png/rec.vtx.elastic_idx"][:]
  npng = f["rec.vtx.elastic.fuzzyk.png/rec.vtx.elastic.fuzzyk.png_idx"][:]
  nhit = f["rec.vtx.elastic.fuzzyk.png/nhit"][:]
  nslc = len(r)
  assert(len(sr) == nslc)
  assert(len(ev) == nslc)
  assert(len(sev) == nslc)
  assert(len(nelastic) == nslc)
  assert(len(npng) == nslc)
  assert(len(nhit) == nslc)

  with open("%d_%d_vtx_elastic_fuzzyk_png" % (r[0],sr[0]), "w") as of:
    for i in xrange(nslc):
      of.write("%d\t%d\t%d\t%d\t%d\t%d\t%d\n" % (r[i], sr[i], ev[i], sev[i], nelastic[i], npng[i],nhit[0]))

def write_elastic_fuzzyk_shwlid(f):
  r = f["rec.vtx.elastic.fuzzyk.png.shwlid/run"][:]
  assert(np.all(r==r[0]))
  sr = f["rec.vtx.elastic.fuzzyk.png.shwlid/subrun"][:]
  assert(np.all(sr==sr[0]))
  ev = f["rec.vtx.elastic.fuzzyk.png.shwlid/evt"][:]
  sev = f["rec.vtx.elastic.fuzzyk.png.shwlid/subevt"][:]
  nelastic = f["rec.vtx.elastic.fuzzyk.png.shwlid/rec.vtx.elastic_idx"][:]
  nshwlid = f["rec.vtx.elastic.fuzzyk.png.shwlid/rec.vtx.elastic.fuzzyk.png_idx"][:]
  nplane = f["rec.vtx.elastic.fuzzyk.png.shwlid/nplane"][:]
  nslc = len(r)
  assert(len(sr) == nslc)
  assert(len(ev) == nslc)
  assert(len(sev) == nslc)
  assert(len(nelastic) == nslc)
  assert(len(nshwlid) == nslc)
  assert(len(nplane) == nslc)

  with open("%d_%d_vtx_elastic_fuzzyk_png_shwlid" % (r[0],sr[0]), "w") as of:
    for i in xrange(nslc):
      of.write("%d\t%d\t%d\t%d\t%d\t%d\t%d\n" % (r[i], sr[i], ev[i], sev[i], nelastic[i], nshwlid[i],nplane[i]))


def write_elastic_fuzzyk_cvnpart(f):
  r = f["rec.vtx.elastic.fuzzyk.png.cvnpart/run"][:]
  assert(np.all(r==r[0]))
  sr = f["rec.vtx.elastic.fuzzyk.png.cvnpart/subrun"][:]
  assert(np.all(sr==sr[0]))
  ev = f["rec.vtx.elastic.fuzzyk.png.cvnpart/evt"][:]
  sev = f["rec.vtx.elastic.fuzzyk.png.cvnpart/subevt"][:]
  nelastic = f["rec.vtx.elastic.fuzzyk.png.cvnpart/rec.vtx.elastic_idx"][:]
  ncvnpart = f["rec.vtx.elastic.fuzzyk.png.cvnpart/rec.vtx.elastic.fuzzyk.png_idx"][:]
  pdgmax = f["rec.vtx.elastic.fuzzyk.png.cvnpart/pdgmax"][:]
  nslc = len(r)
  assert(len(sr) == nslc)
  assert(len(ev) == nslc)
  assert(len(sev) == nslc)
  assert(len(nelastic) == nslc)
  assert(len(ncvnpart) == nslc)
  assert(len(pdgmax) == nslc)

  with open("%d_%d_vtx_elastic_fuzzyk_png_cvnpart" % (r[0],sr[0]), "w") as of:
    for i in xrange(nslc):
      of.write("%d\t%d\t%d\t%d\t%d\t%d\t%d\n" % (r[i], sr[i], ev[i], sev[i], nelastic[i], ncvnpart[i],pdgmax[i]))

def process_file(fname):
  f = h5.File(fname)
  write_hdr(f)
  write_vtx(f)
  write_slc(f)
  write_elastic_fuzzyk(f)
  write_elastic_fuzzyk_png(f)
  write_elastic_fuzzyk_shwlid(f)
  write_elastic_fuzzyk_cvnpart(f)

if __name__ == "__main__":
  fname = sys.argv[1]
  process_file(fname)
  print "KTHXBYE"
