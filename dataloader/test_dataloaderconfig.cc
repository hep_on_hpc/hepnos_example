#include "../code/catch.hpp"

#include <DataLoaderConfig.hpp>

#include "diy/serialization.hpp"

TEST_CASE("STRING")
{
  SECTION("big string")
  {
    diy::MemoryBuffer mb;
    std::string s("abc "
                  "s;ldfjkn;avfvbyfgvryrvg6rgvryvgy6rgv6vg6rrvq'iwfjf;nasnv;"
                  "sdfj\\nv;asfva;ds\\t\\n;cv");
    diy::save(mb, s.size());
    diy::save(mb, s.data(), s.size());
    mb.reset();
    std::size_t sz = 0;
    std::string in;
    diy::load(mb, sz);
    in.resize(sz);
    diy::load(mb, in.data(), sz);
    CHECK(sz == s.size());
    CHECK(in == s);
  }

  SECTION("small string")
  {
    diy::MemoryBuffer mb;
    std::string s("abc");
    diy::save(mb, s.size());
    diy::save(mb, s.data(), s.size());
    mb.reset();
    std::size_t sz = 0;
    std::string in;
    diy::load(mb, sz);
    in.resize(sz);
    diy::load(mb, in.data(), sz);
    CHECK(sz == s.size());
    CHECK(in == s);
  }

  SECTION("empty string")
  {
    diy::MemoryBuffer mb;
    std::string s;
    diy::save(mb, s.size());
    diy::save(mb, s.data(), s.size());
    mb.reset();
    std::size_t sz = 0;
    std::string in;
    diy::load(mb, sz);
    in.resize(sz);
    diy::load(mb, in.data(), sz);
    CHECK(sz == s.size());
    CHECK(in == s);
  }
}

TEST_CASE("DLC")
{
  /*  SECTION("small string") {
   *
      diy::MemoryBuffer mb;
      DataLoaderConfig dlc("abcdefgh");
      diy::save(mb, dlc);
      mb.reset();
      DataLoaderConfig in;
      diy::load(mb, in);
      CHECK(in.filename == dlc.filename);
    }

    SECTION("big string") {
      diy::MemoryBuffer mb;
      DataLoaderConfig dlc(std::string(30,'x'));
      diy::save(mb, dlc);
      mb.reset();
      DataLoaderConfig in;
      diy::load(mb, in);
      CHECK(in.filename == dlc.filename);
    }
  */
  SECTION("vector of string")
  {
    diy::MemoryBuffer mb;
    // std::string s("abc");
    std::vector<std::string> s{"cow", "dog", "hen"};
    std::vector<std::string> s1{"pig", "dog", "hen"};
    diy::save(mb, s.size());
    diy::save(mb, s.data(), s.size());
    mb.reset();
    std::size_t sz = 0;
    std::vector<std::string> in;
    diy::load(mb, sz);
    in.resize(sz);
    diy::load(mb, in.data(), sz);
    CHECK(sz == s.size());
    CHECK(in == s);
    CHECK(s1 == s);
  }
}
