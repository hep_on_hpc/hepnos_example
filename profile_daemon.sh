source /cvmfs/nova.opensciencegrid.org/externals/setups
setup forge_tools v19_0_3
spack load -r hepnos
cd /hepnos_example
map --profile hepnos-daemon hepnos-daemon-config.yaml /tmp/client-config.yaml &
cd /hepnos_example/dataloader/build
mpirun -np 1 ./dataloader /tmp/client-config.yaml ../onefile.txt
hepnos-shutdown /tmp/client-config.yaml
