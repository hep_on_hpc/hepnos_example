#include "catch.hpp"
#include "make_track.h"
#include "track_serialization.h"
#include "lardataobj/RecoBase/Track.h"
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/version.hpp>
#include <fstream>

TEST_CASE("writing a track works")
{
  auto const t = recob::make_track(2);
  auto const t4 = recob::make_track(2);
  CHECK((t4 == t));
  {
    std::ofstream ofs("trackout");
    boost::archive::binary_oarchive oa(ofs);
    oa << t;
    CHECK(ofs.good());
    ofs.close();
  }
    std::ifstream ifs("trackout");
    boost::archive::binary_iarchive ia(ifs);
    CHECK(ifs.good());
    recob::Track t2;
    CHECK(t2 == recob::Track());
    ia >> t2;
    CHECK(t2 == t);
}

TEST_CASE("writing a vector<track> works")
{
  std::vector<recob::Track> tracks {recob::make_track(1), recob::make_track(2)};
  {
    std::ofstream ofs("tracksout");
    boost::archive::binary_oarchive oa(ofs);
    oa << tracks;
    CHECK(ofs.good());
    ofs.close();
  }
  std::ifstream ifs("tracksout");
  CHECK(ifs.good());
  boost::archive::binary_iarchive ia(ifs);
  std::vector<recob::Track> tracks2 {recob::make_track(1), recob::make_track(3)};
  CHECK(!(tracks == tracks2));
  ia >> tracks2;
  CHECK(tracks == tracks2);
}
    
