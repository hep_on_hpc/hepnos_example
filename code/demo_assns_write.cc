#include <iostream>
#include <string>
#include <vector>

#include "canvas/Persistency/Common/FindMany.h"
#include "gallery/Event.h"
#include "lardataobj/RecoBase/Hit.h"

#include <hepnos/DataSet.hpp>
#include <hepnos/DataStore.hpp>
#include <hepnos/Run.hpp>
#include <hepnos/RunSet.hpp>

#include "hit_serialization.h"
#include "track_serialization.h"
#include "std_pair_serialization.h"

using namespace std;

namespace hepnos { 
  template <typename A, typename B>
  using Assns =std::vector<std::pair<hepnos::Ptr<A>, hepnos::Ptr<B>>>; 
}

// We use a function try block to catch and report on all exceptions.
int
main(int argc, char** argv) try {
  if (argc <= 2) {
    cout << "Please specify the path to the <hepnos-client.yaml> "
            "and name of one or more art/ROOT input file(s) to read.\n";
    return 1;
  }

  string const configfile(*(argv + 1));
  vector<string> const filenames(argv + 2, argv + argc);

  gallery::Event ev(filenames);
  cout << ev.numberOfEventsInFile() << "\n";

  hepnos::DataStore datastore(configfile);
  auto ds1 = datastore.createDataSet("Fermilab");
  auto ds2 = ds1.createDataSet("Epoch1");

  art::RunID current_run_id;
  art::SubRunID current_subrun_id;

  hepnos::Run current_run;
  hepnos::SubRun current_subrun;

  for (gallery::Event ev(filenames); !ev.atEnd(); ev.next()) {
    auto aux = ev.eventAuxiliary();
    if (current_run_id != aux.runID()) {
      current_run_id = aux.runID();
      current_run = ds2.createRun(current_run_id.run());
      current_subrun_id = aux.subRunID();
      current_subrun = current_run.createSubRun(current_subrun_id.subRun());
    } else if (current_subrun_id != aux.subRunID()) {
      current_subrun_id = aux.subRunID();
      current_subrun = current_run.createSubRun(current_subrun_id.subRun());
    }
    hepnos::Event current_ev = current_subrun.createEvent(aux.event());

    art::InputTag const hit_tag("gaushit", "", "Reco");
    auto const hit_handle = ev.getValidHandle<vector<recob::Hit>>(hit_tag);
    auto const hits_id = current_ev.store(hit_tag, *hit_handle);

    art::InputTag const trk_tag("pmtrack", "", "Reco");
    auto const trk_handle = ev.getValidHandle<vector<recob::Track>>(trk_tag); 
    auto const trks_id = current_ev.store(trk_tag, *trk_handle);

    art::InputTag const assns_tag("pmtrack", "", "Reco");
    auto const & assns = *ev.getValidHandle<art::Assns<recob::Hit, recob::Track>>(assns_tag);
    
    hepnos::Assns<recob::Hit, recob::Track> h_assns;
    h_assns.reserve(assns.size());

    for (auto const & a: assns) {
      std::cout << a.first.key() << "," << a.second.key() << "\n";
      auto const hit_ptr = datastore.makePtr<recob::Hit>(hits_id, a.first.key());
      auto const trk_ptr = datastore.makePtr<recob::Track>(trks_id, a.second.key());
      h_assns.push_back(make_pair(hit_ptr, trk_ptr)); 
    }

    current_ev.store(assns_tag, h_assns); 

  }
}
catch (exception const& ex) {
  cerr << ex.what() << '\n';
  return 1;
}
catch (...) {
  cerr << "Unidentifiable exception caught\n";
  return 1;
}
