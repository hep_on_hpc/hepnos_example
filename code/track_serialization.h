#include "larcoreobj/SimpleTypesAndConstants/geo_vectors.h"
#include "lardataobj/RecoBase/Track.h"

#include <iostream>

#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/version.hpp>

#include "Math/SMatrix.h"

namespace boost {
  namespace serialization {
    //using SMatrixSym55 = ROOT::Math::SMatrix<double,5,5,ROOT::Math::MatRepSym<double,5> >;
    using SMatrixSym55 = recob::tracking::SMatrixSym55; 
    using Scalar_p = recob::tracking::Point_t::Scalar;
    using Scalar_v = recob::tracking::Vector_t::Scalar;

    template <class Archive>
    void
    save(Archive& ar, const recob::Track& t, const unsigned int)
    {
      auto pos = t.Trajectory().Trajectory().Positions(); 
      size_t sz= pos.size();
      ar << sz;
      for (size_t i = 0; i<sz; ++i) {
        ar << pos[i].X();
        ar << pos[i].Y();
        ar << pos[i].Z();
      } 
      auto mom = t.Trajectory().Trajectory().Momenta();
      size_t sz1 = mom.size();
      ar << sz1;
      for (size_t i = 0; i<sz; ++i) {
        ar << mom[i].X();
        ar << mom[i].Y();
        ar << mom[i].Z();
      }
      ar << t.Trajectory().Trajectory().HasMomentum();
      auto flags = t.Trajectory().Flags();
      size_t flags_sz = flags.size();
      ar << flags_sz;
      for (size_t i=0; i< flags_sz; ++i) {
        ar << flags[i].fromHit();
      } 
      ar << t.ParticleId();
      ar << t.Chi2();
      ar << t. Ndof();
      auto const aa = t.VertexCovarianceLocal5D();
      for (auto a = aa.begin(); a!= aa.end(); ++a) {
        ar << *a;
      }
      auto const bb = t.EndCovarianceLocal5D();
      for (auto b=bb.begin(); b!=bb.end(); ++b) {
        ar << *b;
     }
      ar << t.ID();    
    }

    template <class Archive> 
    void
    load(Archive& ar, recob::Track& t, const unsigned int)
    {
      //recob::TrackTrajectory traj; ///< Stored trajectory data member
      int pid;                     ///< Particle ID hypothesis used in the fit (if any)
      float chi2;                  ///< Fit chi2
      int ndof;                    ///< Number of degrees of freedom of the fit
      //SMatrixSym55 covvertex;      ///< Covariance matrix (local 5D) at start point (vertex)
      //SMatrixSym55 covend;         ///< Covariance matrix (local 5D) at end point
      int    id;                   ///< track's ID
      //deprecated:
     // std::vector< std::vector <double> > fdQdx; ///< charge deposition per unit length at points
      bool hasMomenta;

      size_t sz; //size of positions
      ar >> sz;
      //construct positions_t => std::vector<Point_t>
      Scalar_p x, y, z;
      std::vector<geo::Point_t> pos;
      pos.reserve(sz); 
      for (size_t i=0; i< sz; ++i) {
        ar >> x >> y >> z;
        pos.push_back(geo::Point_t(x, y, z));  
      }
      //construct TrackTrajectory

      Scalar_v x1, y1, z1;
      ar >> sz; //now read size of momenta
      std::vector<geo::Vector_t> mom;
      mom.reserve(sz);
      for (size_t i=0; i<sz;++i) {
        ar >> x1 >> y1 >> z1;
        mom.push_back(geo::Vector_t(x1, y1, z1));
      }

      ar >> hasMomenta; 
      //size_t flags_sz;
      ar >> sz;  //now get size of flags
    
      unsigned int hit_index;
      std::vector<recob::TrajectoryPointFlags> flags;
      flags.reserve(sz);
      for (size_t i = 0; i< sz; ++i) {
        ar >> hit_index;
        flags.push_back(recob::TrajectoryPointFlags(hit_index));
      }

      recob::TrackTrajectory traj(std::move(pos), std::move(mom), std::move(flags), hasMomenta);
      ar >> pid >> chi2 >> ndof;

      // to construct Covariance matrix 5x5      
      double d;
      std::vector<double> aa;
      aa.reserve(30);
      for (size_t i=0; i<30 ;++i) {
        ar >> d; 
        aa.push_back(d);
      }
      SMatrixSym55 covvertex(aa.begin(), aa.begin()+15, true, true);
      SMatrixSym55 covend(aa.begin()+15, aa.end(), true, true);
      ar >> id;

      recob::Track t2(traj, pid, chi2, ndof, covvertex, covend, id);
      t = t2; //make sure this works!
    }
  }
}

BOOST_SERIALIZATION_SPLIT_FREE(recob::Track)
