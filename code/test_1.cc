#include <iostream>
#include <string>
#include <vector>

#include "gallery/Event.h"
#include "lardataobj/RecoBase/Hit.h"

#include <hepnos/DataSet.hpp>
#include <hepnos/DataStore.hpp>
#include <hepnos/Run.hpp>
#include <hepnos/RunSet.hpp>

#include "hit_serialization.h"

using namespace hepnos;
using namespace std;

// We use a function try block to catch and report on all exceptions.
int
main(int argc, char** argv) try {
  if (argc <= 2) {
    cout << "Please specify the path to the <hepnos-config.yaml> "
            "and name of one or more art/ROOT input file(s) to read.\n";
    return 1;
  }

  string const configfile(*(argv + 1));
  vector<string> const filenames(argv + 2, argv + argc);

  gallery::Event ev(filenames);
  cout << ev.numberOfEventsInFile() << "\n";

  hepnos::DataStore datastore(configfile);
  auto ds1 = datastore.createDataSet("Fermilab");
  auto ds2 = ds1.createDataSet("Epoch1");

  art::RunID current_run_id;
  art::SubRunID current_subrun_id;

  hepnos::Run current_run;
  hepnos::SubRun current_subrun;

  for (gallery::Event ev(filenames); !ev.atEnd(); ev.next()) {
    auto aux = ev.eventAuxiliary();
    if (current_run_id != aux.runID()) {
      current_run_id = aux.runID();
      current_run = ds2.createRun(current_run_id.run());
      current_subrun_id = aux.subRunID();
      current_subrun = current_run.createSubRun(current_subrun_id.subRun());
    } else if (current_subrun_id != aux.subRunID()) {
      current_subrun_id = aux.subRunID();
      current_subrun = current_run.createSubRun(current_subrun_id.subRun());
    }
    hepnos::Event current_ev = current_subrun.createEvent(aux.event());
    art::InputTag hit_tag("gaushit", "", "PrimaryReco");
    auto const handle = ev.getValidHandle<vector<recob::Hit>>(hit_tag);
    current_ev.store(hit_tag, *handle);

  }
}
catch (exception const& ex) {
  cerr << ex.what() << '\n';
  return 1;
}
catch (...) {
  cerr << "Unidentifiable exception caught\n";
  return 1;
}
