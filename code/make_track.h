//
// This code generates hits and tracks objects that 
// we can use in testing 
//
//

#include "lardataobj/RecoBase/Track.h"
#include "Math/SMatrix.h"

#include <iostream>

using namespace std;

namespace recob
{
recob::Track make_track (int p) {
  using SMatrixSym55 = recob::tracking::SMatrixSym55;
  int pid = 9;
  float chi2 = 2.1;
  int ndof = 3;
  int id = p;
  std::vector<geo::Point_t> pos;
  pos.reserve(5);

  for (size_t i = 0; i<5 ;++i) {
    pos.push_back(geo::Point_t(1., 2., 3.));
  }

  std::vector<geo::Vector_t> mom;
  mom.reserve(5);
  for (size_t i=0; i<5;++i)
    mom.push_back(geo::Vector_t(4., 5., 6.));

  std::vector<geo::Vector_t> mom2{geo::Vector_t(4., 4., 6.), geo::Vector_t(4., 5., 6.), geo::Vector_t(4., 5., 6.), geo::Vector_t(4., 5., 6.), geo::Vector_t(4., 5., 6.)};
 
  size_t g=0;
  for (size_t k=0; k<5;++k)
    if(mom[k]==mom2[k]) ++g;

  bool hasMomenta = true;
  std::vector<recob::TrajectoryPointFlags> flags;
  flags.reserve(5);
  for (size_t i=0; i<5;++i)
    flags.push_back(recob::TrajectoryPointFlags(66));

  recob::TrackTrajectory traj(std::move(pos), std::move(mom), std::move(flags), hasMomenta);

  std::vector<double> aa;
  std::vector<double> bb;
  aa.reserve(15);
  bb.reserve(15);
  for (size_t i=0; i<15; ++i) {
    aa.push_back(i*p);
    bb.push_back(i+2);
  }
  SMatrixSym55 covvertex(aa.begin(), aa.end(), true, true);
  SMatrixSym55 covend(bb.begin(), bb.end(), true, true);
  recob::Track t(traj, pid, chi2, ndof, covvertex, covend, id);
  return t;
}

  inline bool
  operator==(Track const& a, Track const& b)
  {
    return (a.ParticleId() == b.ParticleId())
        && (a.Ndof() == b.Ndof())
        && (a.Chi2() == b.Chi2())
        && (a.ID() == b.ID())
        && (a.StartCovariance() == b.StartCovariance())
        && (a.EndCovariance() == b.EndCovariance())
        && (a.Trajectory().Flags() == b.Trajectory().Flags())
        && (a.Trajectory().Trajectory().HasMomentum() == b.Trajectory().Trajectory().HasMomentum())
        && (a.Trajectory().Trajectory().Positions() == b.Trajectory().Trajectory().Positions())
        && (a.Trajectory().Trajectory().Momenta() == b.Trajectory().Trajectory().Momenta());
  }
}
