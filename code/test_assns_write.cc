// Code to write assns to HEPnOS store
// create a vector of two tracks, and a vector of 6 hits
// add these to the hepnos store, i.e. ev.store(...)
// create an assns collection i.e. 
// std::vector<std::pair<hepnos::Ptr<A>, hepnos::Ptr<B>>>;
// of size 5 between 2 tracks and 5 hits, 
// hits at index 0 and 2 are associated with track at index 0, and 
// hits at index 1, 3, and 5 are associated with track at index 1. 
// Add the vector representing assns to the hepnos store
// This code demonstrates how to write an assns collection 
// to the hepnos store. How to correctly read this data is 
// immplemented in test_assns_read.cc, where we do verify that 
// the data is read back corrrecttly


#include "canvas/Persistency/Common/Assns.h"
#include "lardataobj/RecoBase/Hit.h"
#include "lardataobj/RecoBase/Track.h"

#include "hit_serialization.h"
#include "make_hit.h"
#include "make_track.h"
#include "track_serialization.h" 

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/version.hpp>

#include <hepnos/DataSet.hpp>
#include <hepnos/DataStore.hpp>
#include <hepnos/Run.hpp>
#include <hepnos/RunSet.hpp>

#include <fstream> 
#include <iostream>
#include <type_traits>

namespace hepnos {
  template <typename A, typename B>
  using Assns = std::vector<std::pair<hepnos::Ptr<A>, hepnos::Ptr<B>>>;
}

int main () {
  hepnos::DataStore datastore("client.yaml");
  auto ds1 = datastore.createDataSet("Fermilab");
  auto ds2 = ds1.createDataSet("Epoch2"); 
  auto r = ds2.createRun(1000); 
  auto sr = r.createSubRun(34);
  hepnos::Event ev = sr.createEvent(100); 

  std::vector<recob::Track> tracks { recob::make_track(11), 
                                     recob::make_track(12) };
  auto const trks_id = ev.store("tracks", tracks);

  std::vector<recob::Hit> hits { make_hit(1), 
                                 make_hit(2), 
                                 make_hit(3), 
                                 make_hit(4), 
                                 make_hit(5), 
                                 make_hit(6)};
  auto const hits_id = ev.store("hits", hits);

  hepnos::Assns<recob::Hit, recob::Track> assns;
  assns.reserve(5);
  std::vector<size_t> t {0, 0, 1, 1, 1};
  std::vector<size_t> h {0, 2, 1, 3, 5};
  for (size_t i = 0; i < 5; ++i) {
    auto p1 = datastore.makePtr<recob::Hit>(hits_id, h[i]); 
    auto p2 = datastore.makePtr<recob::Track>(trks_id, t[i]); 
    assns.push_back(std::make_pair(p1, p2)); 
 }
 ev.store("assns", assns);
 return 0;
}
