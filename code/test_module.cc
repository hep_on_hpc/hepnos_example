#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

//#include "gallery/Event.h"
#include "lardataobj/RecoBase/Hit.h"
#include "lardataobj/RecoBase/Track.h"

#include <hepnos/DataSet.hpp>
#include <hepnos/DataStore.hpp>
#include <hepnos/Run.hpp>
#include <hepnos/RunSet.hpp>

#include "hit_serialization.h"
#include "track_serialization.h"
#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>

#include "make_track.h"
#include "make_hit.h"
#include "compare_hit.h"

class Mymodule {
  public: 
    void beginRun(hepnos::Run& r);
    void endRun(hepnos::Run& r);
    void beginSubRun(hepnos::SubRun& sr);
    void endSubRun(hepnos::SubRun& sr);
    void processEvent(hepnos::Event& e);
}

void Mymodule::processEvent(hepnos::Event& e) {
     cout << "Event Number: " << e.number() << endl;
            std::vector<std::pair<hepnos::Ptr<recob::Hit>, hepnos::Ptr<recob::Track>>> assns;
            cout << "Checking assns\n";
            e.load("assns", assns);
            for (size_t i = 0; i<5 ;++i) {
              CHECK(*assns[i].second==recob::make_track(t[i]));
              CHECK(*(assns[i].first)==make_hit(h[i]));
            }
            cout << "Checking hits\n";
            std::vector<recob::Hit> hits;
            e.load("hits", hits);
            int k = 1;
            for (auto const& h : hits) {
              CHECK(h==make_hit(k));
              ++k;
            }

}

void Mymodule::beginRun(hepnos::Run& r) {
        cout << "Run Number: " << r.number() << endl;
}
TEST_CASE("reading back assns work")
{
  Mymodule mod;
  std::vector<int> t {11, 11, 12, 12, 12};
  std::vector<int> h {1, 3, 2, 4, 6};
  hepnos::DataStore datastore("client.yaml");
  for (auto it = datastore.begin(); it != datastore.end(); ++it) {
    cout << "Dataset from iterator: " << it->fullname() << std::endl;
    for (auto & ds : *it) {
      cout << "Dataset from iterator: " << ds.fullname() << std::endl;
      for (auto & r : ds.runs()) {
        mod.beginRun(r);
        for (auto & sr : r) {
          cout << "SubRun Number: " << sr.number() << endl;
          for (auto & e : sr) {
            mod.processEvent(e);
          } //end event
        } // end subrun
        mod.endRun(r);
      }// end run
    }// end dataset
  } // end datastore
}

