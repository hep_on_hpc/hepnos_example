#include <iostream>
#include <string>
#include <vector>

#include "gallery/Event.h"
#include "lardataobj/RecoBase/Hit.h"

#include <hepnos/DataSet.hpp>
#include <hepnos/DataStore.hpp>
#include <hepnos/Run.hpp>
#include <hepnos/RunSet.hpp>

#include "hit_serialization.h"

using namespace hepnos;
using namespace std;

int
main(int argc, char** argv) try {
  if (argc <= 1) {
    cout << "Please specify the path to the <hepnos-config.yaml> \n";
    return 1;
  }
  string const configfile(*(argv + 1));
  hepnos::DataStore datastore(configfile);
  for (auto it = datastore.begin(); it != datastore.end(); ++it) {
    cout << "Dataset from iterator: " << it->fullname() << std::endl;
    for (auto const& ds : *it) {
      cout << "Dataset from iterator: " << ds.fullname() << std::endl;
      for (auto const& r : ds.runs()) {
        cout << "Run Number: " << r.number() << endl;
        for (auto const& sr : r) {
          cout << "SubRun Number: " << sr.number() << endl;
          for (auto const& e : sr) {
            cout << "Event Number: " << e.number() << endl;
            std::vector<recob::Hit> hits;
            art::InputTag hit_tag("gaushit", "", "PrimaryReco");
            e.load(hit_tag, hits);
            for (auto const& h : hits) {
              cout << h << endl;
            }
          }
        }
      }
    }
  }
}
catch (std::exception const& ex) {
  cerr << ex.what() << '\n';
  return 1;
}
catch (...) {
  cerr << "Unidentifiable exception caught\n";
  return 1;
}
